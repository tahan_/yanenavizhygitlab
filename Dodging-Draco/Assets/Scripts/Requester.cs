﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Requester : MonoBehaviour
{
    [SerializeField] private string url;
    
    private UniWebView webView;

    private readonly string FIRST_LOGIN = "FirstLogin";
    private readonly string LEAD = "lead";
    private readonly string END_POINT = "endpoint";
    
    private void Start()
    {
        if(Convert.ToBoolean(PlayerPrefs.GetInt(FIRST_LOGIN, 1)))
        {
            StartCoroutine(SendRequest());
        }
        else
        {
            if (Convert.ToBoolean(PlayerPrefs.GetInt(LEAD)))
            {
                OpenWebWiew(PlayerPrefs.GetString(END_POINT));
            }
            else
            {
                StartGame();
            }
        }
    }

    private void OpenWebWiew(string url)
    {
        var webViewGameObject = new GameObject("UniWebView");
        webView = webViewGameObject.AddComponent<UniWebView>();
        webView.Frame = new Rect(0, 0, Screen.width, Screen.height);
        webView.Load(url);
        webView.Show();
    }
    
    private void StartGame()
    {
        SceneManager.LoadScene(1);
    }
    
    private IEnumerator SendRequest()
    {
        PlayerPrefs.SetInt(FIRST_LOGIN, 0);
        
        UnityWebRequest request = UnityWebRequest.Get(url);

        yield return request.SendWebRequest();

        if (request.result == UnityWebRequest.Result.Success && !string.IsNullOrEmpty(request.downloadHandler.text))
        {
            PlayerPrefs.SetInt(LEAD, 1);
            PlayerPrefs.SetString(END_POINT,request.downloadHandler.text);
            OpenWebWiew(request.downloadHandler.text);
        }
        else
        {
            PlayerPrefs.SetInt(LEAD, 0);
            StartGame();
        }
    }
}
