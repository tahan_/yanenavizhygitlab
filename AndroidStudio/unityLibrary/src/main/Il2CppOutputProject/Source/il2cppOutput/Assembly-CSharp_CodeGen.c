﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x00000002 System.Void GameManager::GameOver()
extern void GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A (void);
// 0x00000003 System.Void GameManager::IncrementScore()
extern void GameManager_IncrementScore_m290C1A32CC6750C955D73EDF8CA0A69477F61972 (void);
// 0x00000004 System.Void GameManager::Restart()
extern void GameManager_Restart_m1E9741B5443E1FF65B0BE1A20A8F584C90394654 (void);
// 0x00000005 System.Void GameManager::Menu()
extern void GameManager_Menu_m8EC24AFAB7F645E62E7E45A8E30BD41C2BEA1A14 (void);
// 0x00000006 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000007 System.Void MenuController::Play()
extern void MenuController_Play_m530798B66FC38041D527B944A73021DD3593C584 (void);
// 0x00000008 System.Void MenuController::Quit()
extern void MenuController_Quit_mAA57BD6D0000980B89F6AEE06C039954A2CADDA1 (void);
// 0x00000009 System.Void MenuController::.ctor()
extern void MenuController__ctor_m59357650A99D124D2EB1B4AD34FD6EB2B5F6E182 (void);
// 0x0000000A System.Void Obstacle::Start()
extern void Obstacle_Start_mB31D51F665C9770319F028D5F370D5F1B39EF370 (void);
// 0x0000000B System.Void Obstacle::Update()
extern void Obstacle_Update_m926110BAC1589369A867CEEDA4BB1A8BF553415B (void);
// 0x0000000C System.Void Obstacle::FixedUpdate()
extern void Obstacle_FixedUpdate_mDFBD659D3AE3EA2FAA8F9358865F354686DC8D7F (void);
// 0x0000000D System.Void Obstacle::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Obstacle_OnTriggerEnter2D_m1BF4F5B7D0494F2FC5BD499E098F75D5D81243AD (void);
// 0x0000000E System.Void Obstacle::.ctor()
extern void Obstacle__ctor_m8DAB1C8ECB2328D09E320C484CC0C76D7C474C96 (void);
// 0x0000000F System.Void ObstacleSpawner::Start()
extern void ObstacleSpawner_Start_mBD8A272A8EEAD54E0988D0AE0D7663548DA0C745 (void);
// 0x00000010 System.Void ObstacleSpawner::Update()
extern void ObstacleSpawner_Update_mF0679B08839571982405A550397BC9DF86D72080 (void);
// 0x00000011 System.Void ObstacleSpawner::Spawn()
extern void ObstacleSpawner_Spawn_mBCE582C62AFDF69291AC7E216B81772A2706C253 (void);
// 0x00000012 System.Void ObstacleSpawner::StartSpawning()
extern void ObstacleSpawner_StartSpawning_m8EC853164D71C58D37FE4538DD02D9595CD31040 (void);
// 0x00000013 System.Void ObstacleSpawner::StopSpawning()
extern void ObstacleSpawner_StopSpawning_m5722B1E68FC1BEA3BC83407B7B5D72707F1DA891 (void);
// 0x00000014 System.Void ObstacleSpawner::.ctor()
extern void ObstacleSpawner__ctor_m347D7FB8C7FAEB149F02F9A147EE7E51A28F57ED (void);
// 0x00000015 System.Void PlayerContoller::Awake()
extern void PlayerContoller_Awake_m0C08F4CC8D1AC20A8F963C258D8F69B7E03E9F5A (void);
// 0x00000016 System.Void PlayerContoller::Start()
extern void PlayerContoller_Start_m982316875B21E6A453F23FB221478088BC329DE2 (void);
// 0x00000017 System.Void PlayerContoller::Update()
extern void PlayerContoller_Update_mA2A6E6C36D0CF8B2D39CA0A29DBEBF24855C56A3 (void);
// 0x00000018 System.Void PlayerContoller::FixedUpdate()
extern void PlayerContoller_FixedUpdate_m2975237472C8B3BE0972945E3D502449D1372205 (void);
// 0x00000019 System.Void PlayerContoller::.ctor()
extern void PlayerContoller__ctor_m03822ECE4ED0D9F39965D7C73ED6B7230623969C (void);
// 0x0000001A System.Void Requester::Start()
extern void Requester_Start_m8F1BCA215230B4EDCD4B74F21ECA7D43E70FFAFD (void);
// 0x0000001B System.Void Requester::OpenWebWiew(System.String)
extern void Requester_OpenWebWiew_m9BB5805CD49B03943334E93D8C81224616A63B03 (void);
// 0x0000001C System.Void Requester::StartGame()
extern void Requester_StartGame_m0A1E36BCBF1AA5FE3B61E54FF9F702B8E382E86D (void);
// 0x0000001D System.Collections.IEnumerator Requester::SendRequest()
extern void Requester_SendRequest_m60C313E8841546A79EA791AC5422DD360F76D9F0 (void);
// 0x0000001E System.Void Requester::.ctor()
extern void Requester__ctor_m6E5FD25E125B3CE77215AF2191A57011BCCA4E1D (void);
// 0x0000001F System.Void Requester_<SendRequest>d__8::.ctor(System.Int32)
extern void U3CSendRequestU3Ed__8__ctor_mF38B2C87208A0F0BB22EA71D1D4A8904E84D04F7 (void);
// 0x00000020 System.Void Requester_<SendRequest>d__8::System.IDisposable.Dispose()
extern void U3CSendRequestU3Ed__8_System_IDisposable_Dispose_m27490D7CDCD12311729685473DA6B36671F59477 (void);
// 0x00000021 System.Boolean Requester_<SendRequest>d__8::MoveNext()
extern void U3CSendRequestU3Ed__8_MoveNext_m8B4FFBD2C07B4F24A13FC676E54F02071E7C3DF9 (void);
// 0x00000022 System.Object Requester_<SendRequest>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendRequestU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0672F5A3AEA28B29F7FC63EB89D27377EDF4035B (void);
// 0x00000023 System.Void Requester_<SendRequest>d__8::System.Collections.IEnumerator.Reset()
extern void U3CSendRequestU3Ed__8_System_Collections_IEnumerator_Reset_m3265DD08148E758237A91D4A5615F0026EB2CFBB (void);
// 0x00000024 System.Object Requester_<SendRequest>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CSendRequestU3Ed__8_System_Collections_IEnumerator_get_Current_mE8E8C087A181895B0C29BE35AA1D919A53041B72 (void);
static Il2CppMethodPointer s_methodPointers[36] = 
{
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_GameOver_m402A112370B58EBA3B2171FABC09467E1ED28E9A,
	GameManager_IncrementScore_m290C1A32CC6750C955D73EDF8CA0A69477F61972,
	GameManager_Restart_m1E9741B5443E1FF65B0BE1A20A8F584C90394654,
	GameManager_Menu_m8EC24AFAB7F645E62E7E45A8E30BD41C2BEA1A14,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	MenuController_Play_m530798B66FC38041D527B944A73021DD3593C584,
	MenuController_Quit_mAA57BD6D0000980B89F6AEE06C039954A2CADDA1,
	MenuController__ctor_m59357650A99D124D2EB1B4AD34FD6EB2B5F6E182,
	Obstacle_Start_mB31D51F665C9770319F028D5F370D5F1B39EF370,
	Obstacle_Update_m926110BAC1589369A867CEEDA4BB1A8BF553415B,
	Obstacle_FixedUpdate_mDFBD659D3AE3EA2FAA8F9358865F354686DC8D7F,
	Obstacle_OnTriggerEnter2D_m1BF4F5B7D0494F2FC5BD499E098F75D5D81243AD,
	Obstacle__ctor_m8DAB1C8ECB2328D09E320C484CC0C76D7C474C96,
	ObstacleSpawner_Start_mBD8A272A8EEAD54E0988D0AE0D7663548DA0C745,
	ObstacleSpawner_Update_mF0679B08839571982405A550397BC9DF86D72080,
	ObstacleSpawner_Spawn_mBCE582C62AFDF69291AC7E216B81772A2706C253,
	ObstacleSpawner_StartSpawning_m8EC853164D71C58D37FE4538DD02D9595CD31040,
	ObstacleSpawner_StopSpawning_m5722B1E68FC1BEA3BC83407B7B5D72707F1DA891,
	ObstacleSpawner__ctor_m347D7FB8C7FAEB149F02F9A147EE7E51A28F57ED,
	PlayerContoller_Awake_m0C08F4CC8D1AC20A8F963C258D8F69B7E03E9F5A,
	PlayerContoller_Start_m982316875B21E6A453F23FB221478088BC329DE2,
	PlayerContoller_Update_mA2A6E6C36D0CF8B2D39CA0A29DBEBF24855C56A3,
	PlayerContoller_FixedUpdate_m2975237472C8B3BE0972945E3D502449D1372205,
	PlayerContoller__ctor_m03822ECE4ED0D9F39965D7C73ED6B7230623969C,
	Requester_Start_m8F1BCA215230B4EDCD4B74F21ECA7D43E70FFAFD,
	Requester_OpenWebWiew_m9BB5805CD49B03943334E93D8C81224616A63B03,
	Requester_StartGame_m0A1E36BCBF1AA5FE3B61E54FF9F702B8E382E86D,
	Requester_SendRequest_m60C313E8841546A79EA791AC5422DD360F76D9F0,
	Requester__ctor_m6E5FD25E125B3CE77215AF2191A57011BCCA4E1D,
	U3CSendRequestU3Ed__8__ctor_mF38B2C87208A0F0BB22EA71D1D4A8904E84D04F7,
	U3CSendRequestU3Ed__8_System_IDisposable_Dispose_m27490D7CDCD12311729685473DA6B36671F59477,
	U3CSendRequestU3Ed__8_MoveNext_m8B4FFBD2C07B4F24A13FC676E54F02071E7C3DF9,
	U3CSendRequestU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0672F5A3AEA28B29F7FC63EB89D27377EDF4035B,
	U3CSendRequestU3Ed__8_System_Collections_IEnumerator_Reset_m3265DD08148E758237A91D4A5615F0026EB2CFBB,
	U3CSendRequestU3Ed__8_System_Collections_IEnumerator_get_Current_mE8E8C087A181895B0C29BE35AA1D919A53041B72,
};
static const int32_t s_InvokerIndices[36] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	23,
	32,
	23,
	102,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	36,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
